import { NavigationService } from '@/services/navigation'
import { colors } from '@/vars'
import * as React from 'react'
import {
  AppState,
  AppStateStatus,
  AsyncStorage,
  SafeAreaView,
  StyleSheet,
  View,
} from 'react-native'
import {
  NavigationInjectedProps,
  NavigationScreenConfig,
  NavigationStackScreenOptions,
} from 'react-navigation'
import { Subscription } from 'rxjs'
import { withContext } from '@/shared/withContext'
import { AppContext } from '@/screens/App/AppContext'
import { AppContextState } from '@/screens/App/AppContainer'
import { safeLocation } from '@/shared/location'
import { EventDetected } from '@/modals/EventDetected/EventDetected'
import { SafeEvent } from '@/shared/event'
import { findIndex } from 'lodash'
import moment from 'moment'
import { eventStore } from '@/stores/eventStore'
import { Event } from '@/models/global'
import { HomeContent } from '@/screens/Home/Components/HomeContent'
import { ifIphoneX } from '@/shared/devices'

// init state
const initialState = {
  eventList: null,
  eventListDetected: null,
  appState: AppState.currentState,
}

// default props
const defaultProps = {}

// define type
type DefaultProps = typeof defaultProps

type Props = Partial<{}> &
  NavigationInjectedProps<{}> &
  DefaultProps &
  AppContextState

export type State = Readonly<{}> & Readonly<typeof initialState>

@withContext(AppContext.Consumer)
export class HomeScreen extends React.PureComponent<Props, State> {
  _navListener = new NavigationService(this.props.navigation)
  _eventSubscription: Subscription
  _locationSubscription: Subscription
  _modalEventDetected: React.RefObject<EventDetected> = React.createRef()
  _globalEventResults: Realm.Results<Event>

  static navigationOptions: NavigationScreenConfig<
    NavigationStackScreenOptions
  > = () => {
    return {
      header: null,
    }
  }

  readonly state: State = initialState

  async componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange)
    this._navListener.setBarStyle('light-content')
    this.fetchEvent()
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange)
    this._navListener.removeListener()
    this._eventSubscription && this._eventSubscription.unsubscribe()
    this._locationSubscription && this._locationSubscription.unsubscribe()
    this._globalEventResults &&
      this._globalEventResults.removeAllListeners &&
      this._globalEventResults.removeAllListeners()
  }

  handleAppStateChange = (nextAppState: AppStateStatus) => {
    const { appState } = this.state
    if (
      appState.match(/inactive|background/) &&
      nextAppState === 'active' &&
      !safeLocation.denyLocation
    ) {
      safeLocation.checkPermission()
    }
    this.setState({ appState: nextAppState })
  }

  fetchEvent = () => {
    const { globalEventFactory } = this.props
    const [subscription, results] = globalEventFactory.fetch()

    this._globalEventResults = results

    this._eventSubscription = subscription.subscribe(events => {
      this.setState(
        {
          eventList: events,
        },
        () => {
          this.requestCurrentLocation()
        }
      )
    })
  }

  requestCurrentLocation = () => {
    safeLocation.checkPermission()
    this._locationSubscription = safeLocation.subjectCurrentLocation.subscribe(
      data => {
        // @ts-ignore
        const { type } = data
        const { eventList } = this.state

        if (type === 'Get' && eventList) {
          this.checkEventValidate()
        }
      }
    )
  }

  /*
    HOW IT WORK
    This function use to check user have joined an event or not
      + If return TRUE that mean user has joined an event and don't need to
    display other event
      + If return FALSE that mean user hasn't joined any event or current
       event has been remove and need to display other event for user can
       join in
  */
  checkEventValidate = () => {
    this.getEventIdInStorage().then(currentEventId => {
      const { eventList } = this.state

      // if didn't join any event before, then allow to join
      if (!currentEventId) {
        this.showEvent()
        return
      }

      const eventListIndex = findIndex(eventList, event => {
        // @ts-ignore
        return event.id === currentEventId
      })

      /*
        if couldn't found event user has join in event list, that mean user have
        go far away in event list, then remove current event and allow user to
        join another event
      */
      if (eventListIndex === -1) {
        this.removeEvent().catch(() => {})
        this.showEvent()
        return
      }

      const { coordinate } = new SafeEvent(eventList[eventListIndex])

      /*
        if can't get coordinate of event, that mean error data, then remove
        current event and allow user to join another event
      */
      if (!coordinate) {
        this.removeEvent().catch(() => {})
        this.showEvent()
        return
      }

      const meters = safeLocation.calculateDistance({
        latitude: coordinate.latitude,
        longitude: coordinate.longitude,
      })

      /*
        if user if far away from current event, then remove current event
        and allow user to join another event
      */
      if (meters > safeLocation.lengthToEvent) {
        this.removeEvent().catch(() => {})
        this.showEvent()
        return
      }

      eventStore.currentEvent = eventList[eventListIndex]
      eventStore.updateEventSub.next(eventList[eventListIndex])
    })
  }

  removeEvent = async () => {
    await AsyncStorage.removeItem('eventId')
  }

  showEvent = () => {
    const { eventList } = this.state
    const eventListDetected = safeLocation.detectEvent(eventList)

    this.filterEvent(eventListDetected)
  }

  filterEvent = eventList => {
    this.getEventsDateInStorage(eventList).then(eventDateFromStorage => {
      const listEventFilter = eventList.filter(event => {
        const { eventId } = new SafeEvent(event)
        // @ts-ignore
        const index = findIndex(eventDateFromStorage, item => {
          return item[0] === eventId
        })

        if (index === -1 || !eventDateFromStorage[index][1]) return true

        const eventDate = moment(eventDateFromStorage[index][1])

        const currentDate = moment()

        const duration = moment.duration(currentDate.diff(eventDate))

        return duration.hours() >= 24
      })

      this.setState({
        eventListDetected: listEventFilter,
      })

      if (listEventFilter.length > 0 && this._modalEventDetected.current) {
        this._modalEventDetected.current.open()
      }
    })
  }

  getEventsDateInStorage = async eventList => {
    const eventsId = eventList.map(event => {
      const { eventId } = new SafeEvent(event)

      return eventId
    })

    return await AsyncStorage.multiGet(eventsId)
  }

  getEventIdInStorage = async () => {
    return await AsyncStorage.getItem('eventId')
  }

  onPressOpenMenu = () => {
    this.props.navigation.toggleDrawer()
  }

  render() {
    const { eventListDetected } = this.state

    return (
      <View style={styles.container}>
        <View style={styles.safeArea} />
        <HomeContent onPressOpenMenu={this.onPressOpenMenu} />
        <EventDetected
          ref={this._modalEventDetected}
          detectedEventList={eventListDetected}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background_gray,
  },
  safeArea: {
    ...ifIphoneX(
      {
        height: 40,
      },
      {
        height: 30,
      }
    ),
    backgroundColor: colors.primary_blue,
  },
})
