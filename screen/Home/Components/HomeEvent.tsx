import { Event } from '@/models/team'
import { AppContextState } from '@/screens/App/AppContainer'
import * as React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { Subscription } from 'rxjs'
import { eventStore } from '@/stores/eventStore'
import { SafeEvent } from '@/shared/event'
import { colors, fonts, metrics } from '@/vars'
import I18n from '@/i18n'
import Realm from 'realm'
import { findIndex } from 'lodash'
import { withContext } from '@/shared/withContext'
import { AppContext } from '@/screens/App/AppContext'

type Props = {} & AppContextState

type State = Readonly<{
  currentEvent: Event
  dataEventTeam: any
}>

@withContext(AppContext.Consumer)
export class HomeEvent extends React.PureComponent<Props, State> {
  _subscription: Subscription
  _subscriptionEventTeam: Subscription
  _resultsEventTeam: Realm.Results<any>
  _subscriptionCreateEventTeam: Subscription

  readonly state: State = {
    currentEvent: null,
    dataEventTeam: [],
  }

  componentDidMount() {
    this.fetchEventTeam()
    this._subscription = eventStore.updateEventSub.subscribe(event => {
      // @ts-ignore
      this.setState({
        currentEvent: event,
      })

      this.checkEventInTeam(event)
    })
  }

  componentWillUnmount() {
    this._subscription && this._subscription.unsubscribe()
    this._subscriptionEventTeam && this._subscriptionEventTeam.unsubscribe()
    this._resultsEventTeam && this._resultsEventTeam.removeAllListeners()
    this._subscriptionCreateEventTeam &&
      this._subscriptionCreateEventTeam.unsubscribe()
  }

  fetchEventTeam = () => {
    const { eventFactory } = this.props
    const [subscription, results] = eventFactory.fetch()

    this._resultsEventTeam = results

    this._subscriptionEventTeam = subscription.subscribe(eventTeam => {
      this.setState({
        dataEventTeam: eventTeam,
      })
    })
  }

  checkEventInTeam = (event: Event) => {
    const { dataEventTeam } = this.state
    const { eventFactory } = this.props

    const index = findIndex(dataEventTeam, item => {
      const ev1 = new SafeEvent(item)
      const ev2 = new SafeEvent(event)
      return ev1.eventName === ev2.eventName
    })

    if (index === -1) {
      this._subscriptionCreateEventTeam = eventFactory
        .copyGlobalEvent(event)
        .subscribe(data => {
          eventStore.currentEventTeam = data
        })

      return
    }

    // If already has same event then return nothing
    try {
      if (
        eventStore.currentEventTeam &&
        eventStore.currentEventTeam.id === dataEventTeam[index]
      ) {
        return
      }

      eventStore.currentEventTeam = dataEventTeam[index]
    } catch (e) {
      this.setState({
        dataEventTeam: [],
      })
      eventStore.currentEventTeam = null
      eventStore.currentEvent = null
      return
    }
  }

  get supplierAvailableText() {
    const { boothsLength } = new SafeEvent(this.state.currentEvent)

    if (boothsLength > 1) {
      return I18n.t('suppliersAvailable')
    }

    return I18n.t('supplierAvailable')
  }

  render() {
    const { currentEvent } = this.state

    if (!currentEvent) return null

    const {
      eventName,
      logo,
      primaryColor,
      date,
      venueName,
      boothsLength,
    } = new SafeEvent(currentEvent)

    return (
      <View style={[styles.container, { backgroundColor: primaryColor }]}>
        <View style={styles.wrapLeftContent}>
          <Text style={styles.title} numberOfLines={1}>
            {I18n.t('welcomeTo')} {eventName}!
          </Text>

          <Text style={styles.description} numberOfLines={2}>
            {venueName} - {date.startDate}
          </Text>

          <Text style={styles.description} numberOfLines={1}>
            {boothsLength.toString()} {this.supplierAvailableText}
          </Text>
        </View>

        {logo && (
          <View style={styles.wrapRightContent}>
            <Image source={logo} style={styles.icon} resizeMode={'contain'} />
          </View>
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create<any>({
  container: {
    marginTop: 12,
    height: metrics.home_event_content_height,
    flex: 1,
    flexDirection: 'row',
  },
  wrapLeftContent: {
    flex: 1,
    paddingTop: metrics.home_event_edge_margin,
    paddingBottom: metrics.home_event_edge_margin_2,
    paddingLeft: metrics.home_event_edge_margin,
    paddingRight: metrics.home_event_edge_margin_3,
    justifyContent: 'space-between',
  },
  wrapRightContent: {
    width: metrics.home_event_wrap_logo_size,
    justifyContent: 'center',
  },
  title: {
    fontSize: fonts.size.xl,
    fontFamily: fonts.family.SSPSemiBold,
    color: colors.white,
  },
  description: {
    fontSize: fonts.size.m,
    fontFamily: fonts.family.SSPSemiBold,
    color: colors.light_80,
  },
  icon: {
    height: metrics.home_event_logo_size,
    width: metrics.home_event_logo_size,
  },
})
