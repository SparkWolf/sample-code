import { AppContextState } from '@/screens/App/AppContainer'
import * as React from 'react'
import { HomeHeader } from '@/screens/Home/Components/HomeHeader'
import { HomeEvent } from '@/screens/Home/Components/HomeEvent'
import { HomeListProduct } from '@/screens/Home/Components/HomeListProduct'
import { HomeListSupplier } from '@/screens/Home/Components/HomeListSupplier'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

type Props = {
  onPressOpenMenu: () => void
} & AppContextState

type State = Readonly<{}>

export class HomeContent extends React.PureComponent<Props, State> {
  readonly state: State = {}

  render() {
    const { onPressOpenMenu } = this.props

    return (
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        bounces={false}
      >
        <HomeHeader onPressOpenMenu={onPressOpenMenu} />
        <HomeEvent />
        <HomeListProduct />
        <HomeListSupplier />
      </KeyboardAwareScrollView>
    )
  }
}
