import { Team } from '@/models/common'
import { AppContextState } from '@/screens/App/AppContainer'
import { AppContext } from '@/screens/App/AppContext'
import { withContext } from '@/shared/withContext'
import { colors, images } from '@/vars'
import { fonts } from '@/vars/fonts'
import * as React from 'react'
import { StyleSheet } from 'react-native'
import Realm from 'realm'
import { Subscription } from 'rxjs'
import { AHeader } from '@/components/AHeader/AHeader'
import { withNavigation } from 'react-navigation'

type Props = {
  onPressOpenMenu: () => void
} & AppContextState

type State = Readonly<{
  team: Team
  errMsg: string
  loading: boolean
}>

@withContext(AppContext.Consumer)
export class HomeHeader extends React.PureComponent<Props, State> {
  _subscription: Subscription
  _subscriptionSub: Subscription
  _results: Realm.Results<Team>

  readonly state: State = {
    team: null,
    errMsg: '',
    loading: true,
  }

  componentDidMount() {
    const { teamFactory, selectedTeamId } = this.props

    const [subscription, results] = teamFactory.fetchById(selectedTeamId)

    this._results = results

    this._subscription = subscription.subscribe(team => {
      this.setState({
        team,
      })
    })

    this._subscriptionSub = teamFactory.subject().subscribe(value => {
      if (value === Realm.Sync.SubscriptionState.Complete) {
        this.setState({
          loading: false,
        })
      }
    })
  }

  componentWillUnmount() {
    this._subscription && this._subscription.unsubscribe()
    this._subscriptionSub && this._subscriptionSub.unsubscribe()

    this._results && this._results.removeAllListeners()
  }

  render() {
    const { onPressOpenMenu } = this.props
    const { team } = this.state
    const teamName = team && team.name ? team.name : ''

    return (
      <AHeader
        iconLeft={images.menu}
        onPressIconLeft={onPressOpenMenu}
        title={teamName}
        containerStyle={styles.customHeaderContainer}
        titleStyle={styles.customHeaderTitle}
        wrapIconLeftStyle={styles.customHeaderWrapIconLeft}
        iconLeftStyle={styles.customHeaderIconLeft}
      />
    )
  }
}

const styles = StyleSheet.create<any>({
  customHeaderContainer: {
    height: 63,
    backgroundColor: colors.primary_blue,
    paddingTop: 0,
  },
  customHeaderTitle: {
    color: colors.white,
    fontSize: fonts.size.l,
    fontFamily: fonts.family.SSPSemiBold,
  },
  customHeaderWrapIconLeft: {
    justifyContent: 'center',
  },
  customHeaderIconLeft: {
    height: 24,
    width: 24,
    tintColor: colors.white,
  },
})
