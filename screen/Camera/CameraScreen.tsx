import { ACameraTooltip } from '@/components/ACamera/ACameraTooltip'
import { ACameraGrid } from '@/components/ACamera/ACameraGrid'
import { ACameraHeader } from '@/components/ACamera/ACameraHeader'
import { ANetworkStatus } from '@/components/ANetworkStatus/ANetworkStatus'
import I18n from '@/i18n'
import { Project, Supplier } from '@/models/team'
import {
  imageDataProductSubject,
  imageDataSupplierSubject,
  imageKeyUpdate,
  setLoadingSnapImageBusinessCard,
  updateCameraImage,
} from '@/services/global'
import { isIphoneX } from '@/shared/devices'
import { colors, devices, metrics } from '@/vars'
import * as React from 'react'
import {
  Animated,
  Dimensions,
  Platform,
  SafeAreaView,
  StyleSheet,
  View,
} from 'react-native'
import { RNCamera } from 'react-native-camera'
import { NavigationInjectedProps } from 'react-navigation'
import { Dimension, Orientation } from '@/services/dimension'
import { Subscription } from 'rxjs'
import { NavigationService } from '@/services/navigation'
import ImagePicker, {
  Image as ImageResponse,
} from 'react-native-image-crop-picker'
import { CameraMode } from '@/common/constants/CameraMode'
import { debounce } from 'lodash'
import { permission } from '@/shared/permission'
import { settingStore } from '@/stores/settingStore'
import { mergeMap } from 'rxjs/operators'
import { withContext } from '@/shared/withContext'
import { AppContext } from '@/screens/App/AppContext'
import { AppContextState } from '@/screens/App/AppContainer'
import { pushNotification } from '@/shared/pushNotification'
import { lCache } from '@/libs/LCache'
import { fromPromise } from 'rxjs/internal-compatibility'
import { CameraFooter } from '@/screens/Camera/Components/CameraFooter'
import { createProductStore } from '@/stores/createProductStore'
import { cameraStore } from '@/stores/cameraStore'

type Props = Partial<{}> &
  AppContextState &
  NavigationInjectedProps<{
    productIds: string[]
    supplier: Supplier
    project: Project[]
    isAddSupplierPhoto: boolean
    isAddProductPhoto: boolean
    isAddContactPicture: boolean
    isSnapSupplierBusinessCard: boolean
    isSnapSupplierBusinessCardFromCreateProduct: boolean
    maxFiles: number
    cameraMode: CameraMode
  }>

type State = Readonly<{
  flashModeOn: boolean
  mode: CameraMode
  imageData: string[]
  animatedScale: Animated.Value
  dimension: Orientation
  keyChangeFlash: number
}>

const MAX_FILES = 10000
const MAX_FILES_CHOOSE_IN_GALLERY = 20
const MAX_CONTACT_FILE = 1

@withContext(AppContext.Consumer)
export class CameraScreen extends React.PureComponent<Props, State> {
  _camera: React.RefObject<RNCamera> = React.createRef()
  _subscription: Subscription | undefined
  _navListener = new NavigationService(this.props.navigation)
  _imageType = []
  _updateCameraImage: Subscription
  _onSuccessSubscription: Subscription

  _maxFiles: number = MAX_FILES
  _maxFilesChooseInGallery: number = MAX_FILES_CHOOSE_IN_GALLERY
  _isBeingCapture: boolean = false
  _data = []

  readonly state: State = {
    flashModeOn: false,
    mode: CameraMode.Product,
    imageData: [],
    animatedScale: new Animated.Value(0),
    dimension: Dimension.currentMode,
    keyChangeFlash: 0,
  }

  static navigationOptions = {
    header: null,
    tabBarVisible: false,
  }

  componentDidMount() {
    this.init()
    this.initState()
    this.initSubscribe()
  }

  componentWillUnmount() {
    this._updateCameraImage && this._updateCameraImage.unsubscribe()
    this._subscription && this._subscription.unsubscribe()
    this._navListener && this._navListener.removeListener()
    this._onSuccessSubscription && this._onSuccessSubscription.unsubscribe()
  }

  get navigationData() {
    const { navigation } = this.props

    const cameraMode = navigation.getParam('cameraMode', CameraMode.Product)
    const maxFiles = navigation.getParam(
      'maxFiles',
      MAX_FILES_CHOOSE_IN_GALLERY
    )
    const isAddContactPicture = this.props.navigation.getParam(
      'isAddContactPicture',
      false
    )
    const supplier = navigation.getParam('supplier', null)
    const project = navigation.getParam('project', [])
    const isAddSupplierPhoto = navigation.getParam('isAddSupplierPhoto', false)
    const isAddProductPhoto = navigation.getParam('isAddProductPhoto', false)
    const isSnapSupplierBusinessCard = navigation.getParam(
      'isSnapSupplierBusinessCard',
      false
    )

    return {
      maxFiles,
      cameraMode,
      isAddContactPicture,
      supplier,
      project,
      isAddSupplierPhoto,
      isAddProductPhoto,
      isSnapSupplierBusinessCard,
    }
  }

  init = () => {
    this._navListener.setBarStyle('light-content')
  }

  initState = () => {
    const { maxFiles, cameraMode } = this.navigationData

    this._maxFiles = maxFiles === MAX_CONTACT_FILE ? maxFiles : MAX_FILES
    this._maxFilesChooseInGallery = maxFiles

    this.setState({
      mode: cameraMode,
    })
  }

  initSubscribe = () => {
    /**
     * Check dimension on Ipad
     * */
    this._subscription = Dimension.onChange().subscribe(value => {
      const { dimension } = this.state

      if (dimension !== value) {
        // @ts-ignore
        this.setState({
          dimension: value,
        })
      }
    })

    /**
     * Update image Data from gallery
     * */
    this._updateCameraImage = updateCameraImage.subscribe(data => {
      this.setState({ imageData: data.imageData })
    })

    this._onSuccessSubscription = cameraStore.observer.subscribe(data => {
      imageKeyUpdate.next(data)
    })
  }

  onChangeFlash = () => {
    this.setState((prevState: State) => ({
      flashModeOn: !prevState.flashModeOn,
      keyChangeFlash: prevState.keyChangeFlash + 1,
    }))
  }

  onChangeCameraMode = (newMode: CameraMode) => {
    const { mode } = this.state

    if (mode !== CameraMode.Product && mode !== CameraMode.Supplier) {
      this.setState({
        imageData: [],
      })

      lCache.cleanCache()
      cameraStore.clear()
    }

    this._maxFiles =
      newMode === CameraMode.BusinessCard ? MAX_CONTACT_FILE : MAX_FILES

    this._maxFilesChooseInGallery =
      newMode === CameraMode.BusinessCard
        ? MAX_CONTACT_FILE
        : MAX_FILES_CHOOSE_IN_GALLERY

    this.setState({
      mode: newMode,
    })
  }

  onOpenViewMode = () => {
    const { navigate } = this.props.navigation
    const { imageData } = this.state

    if (imageData.length <= 0) return

    const data = cameraStore.data
    navigate('GalleryPictureScreen', {
      imageData,
      imageKey: data,
      imageType: this._imageType,
      isCreateProduct: true,
      callbackEditImage: this.callbackEditImage,
      fromScreen: 'Camera',
    })
  }

  onPressClose = () => {
    const { navigation } = this.props
    const { isAddContactPicture } = this.navigationData

    createProductStore.clear()
    lCache.cleanCache()
    cameraStore.clear()

    isAddContactPicture && setLoadingSnapImageBusinessCard.next(false)

    navigation.goBack()
  }

  callbackEditImage = (imageType: string[]) => {
    this._imageType = imageType
  }

  animatedScaleTiming = (toValue: number) => {
    const { animatedScale } = this.state

    Animated.timing(animatedScale, {
      toValue,
      duration: 250,
      useNativeDriver: true,
    }).start()
  }

  onOpenGalleryStart = () => {
    /**
     * Start animation
     * */
    this.animatedScaleTiming(0)
  }

  onOpenGalleryEnd = (response: ImageResponse[]) => {
    const paths = []
    for (const image of response) {
      paths.push(image.path)
    }

    this.addImageToQueue(paths)

    /**
     * Start animation
     * */
    this.animatedScaleTiming(2)
  }

  onCapture = debounce(async () => {
    try {
      const { mode } = this.state
      const isBusinessCardMode = mode === CameraMode.BusinessCard

      if (this._isBeingCapture || !this._camera.current) return

      /**
       * Start animation
       * */
      this.animatedScaleTiming(0)

      this._isBeingCapture = true

      const options = {
        skipProcessing: true,
        forceUpOrientation: true,
      }

      const data = await this._camera.current.takePictureAsync(options)

      /**
       * Start animation
       * */
      this.animatedScaleTiming(2)

      /**
       * If capture mode is business card then crop the image
       * */
      if (isBusinessCardMode) {
        const optionCropImage = {
          width: devices.isIOS ? 4 : metrics.screen_width - 40,
          height: devices.isIOS ? 2.5 : metrics.screen_width / 2,
          path: data.uri,
        }

        ImagePicker.openCropper(optionCropImage).then(image => {
          this.addImageToQueue([image.path])

          /**
           * Check and save image to camera roll
           * */
          if (settingStore.savePicture) {
            permission.checkPermissionSaveImage(image.path)
          }
        })
      } else {
        this.addImageToQueue([data.uri])

        /**
         * Check and save image to camera roll
         * */
        if (settingStore.savePicture) {
          permission.checkPermissionSaveImage(data.uri)
        }
      }
    } catch (e) {}
  }, 100)

  addImageToQueue = async (uri: string[]) => {
    const { imageData } = this.state
    const { isSnapSupplierBusinessCard } = this.navigationData
    const arrUri = Array.isArray(uri) ? uri : [uri]

    /**
     * Create image type for drawing
     * */
    arrUri.map(() => {
      this._imageType.push('Photo')
    })

    this.setState(
      prevState => ({
        imageData: prevState.imageData.concat(arrUri),
      }),
      () => {
        !isSnapSupplierBusinessCard &&
          lCache.store(arrUri).then(data => {
            cameraStore.put(data)
          })

        if (imageData.length >= this._maxFiles) {
          this.onPressNext()
          return
        }

        this._isBeingCapture = false
      }
    )
  }

  onPressNext = () => {
    const { navigation, contactFactory } = this.props
    const { imageData, mode } = this.state
    const {
      supplier,
      project,
      isAddSupplierPhoto,
      isAddProductPhoto,
      isAddContactPicture,
      isSnapSupplierBusinessCard,
    } = this.navigationData
    const isBusinessCardMode = mode === CameraMode.BusinessCard

    /**
     * Add photo for product
     * */
    if (!isBusinessCardMode && isAddProductPhoto) {
      imageDataProductSubject.next({ imageData, imageType: this._imageType })
      navigation.goBack()
      return
    }

    /**
     * Add photo for supplier
     * */
    if (!isBusinessCardMode && isAddSupplierPhoto) {
      imageDataSupplierSubject.next({ imageData, imageType: this._imageType })
      navigation.goBack()
      return
    }

    /**
     * Add photo for business card when press snap business card in supplier
     * */
    if (isBusinessCardMode && isSnapSupplierBusinessCard && supplier) {
      pushNotification._snapBusinessCardSubscription = fromPromise(
        lCache.storeWithoutSave(imageData)
      )
        .pipe(
          mergeMap(data => {
            return contactFactory.upsert('', {
              supplier,
              name: '',
              jobTitle: '',
              email: '',
              phoneNumber: '',
              imageData: data,
            })
          })
        )
        .subscribe(() => {
          pushNotification.pushNotification(I18n.t('yourBusinessCardWasSaved'))
        })

      navigation.goBack()
      return
    }

    /**
     * Add photo for business card on other case
     * */
    if (isBusinessCardMode) {
      if (isAddContactPicture) {
        navigation.goBack()
        return
      }

      this._navListener.replace('CreateContactScreen', {
        supplier,
        imageData: cameraStore.data,
        newBusinessCard: true,
        createBusinessCard: true,
      })
      return
    }

    /**
     * Add photo for create product
     * */
    if (mode === CameraMode.Product) {
      this._navListener.replace('CreateProductScreen', {
        supplier,
        project,
        imageData,
        imageType: this._imageType,
      })
    }

    /**
     * Clear image data when move to createProductScreen
     * */
    this.setState({
      imageData: [],
    })
  }

  get message() {
    const { mode } = this.state

    switch (mode) {
      case CameraMode.BusinessCard: {
        return I18n.t('makeSureBusinessCard')
      }
      case CameraMode.Product: {
        return I18n.t('takeOnOrMoreProduct')
      }
      case CameraMode.Supplier: {
        return I18n.t('takeOnOrMoreSupplier')
      }
    }
  }

  get flashMode() {
    const { flashModeOn } = this.state

    if (flashModeOn) {
      return RNCamera.Constants.FlashMode.on
    }

    return RNCamera.Constants.FlashMode.off
  }

  render() {
    const { mode, imageData, animatedScale, keyChangeFlash } = this.state
    const { isAddContactPicture, cameraMode } = this.navigationData
    const width = Dimensions.get('screen').width

    return (
      <SafeAreaView style={styles.container}>
        <ACameraHeader
          key={keyChangeFlash}
          onPressClose={this.onPressClose}
          onOpenGalleryStart={this.onOpenGalleryStart}
          onOpenGalleryEnd={this.onOpenGalleryEnd}
          onChangeFlash={this.onChangeFlash}
          flashMode={this.flashMode}
          maxFiles={this._maxFilesChooseInGallery}
          isAddContactPicture={isAddContactPicture}
        />

        <View style={styles.wrapCamera}>
          <ACameraGrid
            height={
              mode === CameraMode.BusinessCard
                ? metrics.business_card_grid_height
                : null
            }
          />

          <RNCamera
            ref={this._camera}
            style={{ width, flex: 1 }}
            flashMode={this.flashMode}
            captureAudio={false}
            type={RNCamera.Constants.Type.back}
            androidCameraPermissionOptions={{
              title: I18n.t('permissionCameraTitle'),
              message: I18n.t('permissionCameraMessage'),
              buttonPositive: I18n.t('ok'),
              buttonNegative: I18n.t('cancel'),
            }}
            ratio={Platform.select({
              ios: '3:4',
              android: '1:1',
            })}
          />

          <ACameraTooltip
            isVisible={imageData.length <= 0}
            message={this.message}
            position={mode === CameraMode.BusinessCard ? 'top' : 'bottom'}
            padding={mode === CameraMode.BusinessCard ? 20 : 0}
          />
        </View>

        <CameraFooter
          mode={mode}
          cameraMode={cameraMode}
          imageData={imageData}
          animatedScale={animatedScale}
          onOpenViewMode={this.onOpenViewMode}
          onPressNext={this.onPressNext}
          onChangeCameraMode={this.onChangeCameraMode}
          onCapture={this.onCapture}
        />

        <ANetworkStatus
          fillAbsolute={isIphoneX()}
          connectedBgColor={colors.transparent}
        />
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create<any>({
  container: {
    flex: 1,
    backgroundColor: colors.dark_grey,
  },
  wrapCamera: {
    flex: 1,
    backgroundColor: colors.white,
  },
  camera: {
    flex: 1,
  },
})
